package kz.beeline.beeplay.entity;

import io.jmix.core.FileRef;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;

@JmixEntity
@Table(name = "PARTNER")
@Entity
public class Partner {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private Long id;

    @InstanceName
    @Column(name = "title")
    private String title;

    @Column(name = "logo")
    @Lob
    private FileRef logo;

    @Column(name = "link")
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public FileRef getLogo() {
        return logo;
    }

    public void setLogo(FileRef logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}