package kz.beeline.beeplay.entity.dto.mapper.impl;

import io.jmix.core.FileRef;
import kz.beeline.beeplay.entity.Image;
import kz.beeline.beeplay.entity.dto.ImageDTO;
import kz.beeline.beeplay.entity.dto.mapper.FileRefMapper;
import kz.beeline.beeplay.entity.dto.mapper.ImageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ImageMapperImpl implements ImageMapper {
    @Autowired
    private final FileRefMapper fileRefMapper;

    public ImageMapperImpl(FileRefMapper fileRefMapper) {
        this.fileRefMapper = fileRefMapper;
    }

    @Override
    public Image toEntity(ImageDTO dto) {
        return null;
    }

    @Override
    public List<Image> toEntity(List<ImageDTO> dtoList) {
        return null;
    }

    @Override
    public List<ImageDTO> toDto(List<Image> entityList) {
        return null;
    }

    @Override
    public void partialUpdate(Image entity, ImageDTO dto) {

    }

    @Override
    public ImageDTO toDto(Image image) {
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setId(image.getId());
        imageDTO.setLarge(fileRefMapper.toDto(image.getLarge()));
        imageDTO.setSmall(fileRefMapper.toDto(image.getSmall()));

        return imageDTO;
    }



    @Override
    public String toDto(FileRef fileRef) {
        return fileRef.toString();
    }
}
