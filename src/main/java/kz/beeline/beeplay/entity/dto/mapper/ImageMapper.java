package kz.beeline.beeplay.entity.dto.mapper;

import io.jmix.core.FileRef;
import kz.beeline.beeplay.entity.Image;
import kz.beeline.beeplay.entity.dto.ImageDTO;
import org.mapstruct.Mapper;

public interface ImageMapper extends EntityMapper<ImageDTO, Image>{
    String toDto(FileRef fileRef);
}
