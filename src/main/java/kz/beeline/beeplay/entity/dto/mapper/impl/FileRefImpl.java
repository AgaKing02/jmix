package kz.beeline.beeplay.entity.dto.mapper.impl;

import io.jmix.core.FileRef;
import kz.beeline.beeplay.entity.dto.mapper.FileRefMapper;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FileRefImpl implements FileRefMapper {
    public static final String SERVER="http://localhost:8080/downloadFile?file=";
    public static final String FILE_SERVER="fs://";
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FileRefImpl.class);

    @Override
    public FileRef toEntity(String dto) {
        return null;
    }

    @Override
    public String toDto(FileRef entity) {
        String path = entity.toString();
        String result = path.replaceFirst(FILE_SERVER,"");
        result=result.substring(0,result.indexOf('?'));
        log.info(result);
        return SERVER+result;
    }

    @Override
    public List<FileRef> toEntity(List<String> dtoList) {
        return null;
    }

    @Override
    public List<String> toDto(List<FileRef> entityList) {
        return null;
    }

    @Override
    public void partialUpdate(FileRef entity, String dto) {

    }
}
