package kz.beeline.beeplay.entity.dto.mapper;

import io.jmix.core.FileRef;

public interface FileRefMapper extends EntityMapper<String, FileRef>{
}
