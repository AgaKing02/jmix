package kz.beeline.beeplay.entity.dto.mapper;

import kz.beeline.beeplay.entity.Contest;
import kz.beeline.beeplay.entity.dto.ContestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { JuryMapper.class, PartnerMapper.class,FileRefMapper.class})
public interface ContestMapper extends EntityMapper<ContestDTO, Contest>{
    @Mapping(target = "jury", source = "jury")
    @Mapping(target = "partners", source = "partners")
    @Mapping(target = "image",source = "image")
    ContestDTO toDto(Contest s);
}
