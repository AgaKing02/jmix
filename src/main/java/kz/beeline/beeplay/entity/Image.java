package kz.beeline.beeplay.entity;

import io.jmix.core.FileRef;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;

@JmixEntity
@Table(name = "IMAGE")
@Entity
public class Image {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private Long id;

    @Column(name = "large")
    @Lob
    private FileRef large;

    @Column(name = "small")
    @Lob
    private FileRef small;

    public FileRef getSmall() {
        return small;
    }

    public void setSmall(FileRef small) {
        this.small = small;
    }

    public FileRef getLarge() {
        return large;
    }

    public void setLarge(FileRef large) {
        this.large = large;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}