package kz.beeline.beeplay.entity;

import io.jmix.core.FileRef;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@JmixEntity
@Table(name = "CONTEST")
@Entity
public class Contest {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private Long id;

    @Column(name = "slug", nullable = false)
    private String slug;

    @InstanceName
    @Column(name = "title")
    private String title;

    @Column(name = "image")
    @Lob
    private FileRef image;

    @Column(name = "contest_start")
    private LocalDate contest_start;

    @Column(name = "contest_end")
    private LocalDate contest_end;

    @Column(name = "results")
    private LocalDate results;

    @Column(name = "short_description")
    private String short_description;

    @Column(name = "about")
    @Lob
    private String about;

    @Column(name = "specification")
    @Lob
    private String specification;

    @JoinTable(name = "contest_partner_Link",
            joinColumns = @JoinColumn(name = "contest_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "partner_id", referencedColumnName = "ID"))
    @ManyToMany
    private List<Partner> partners;

    @JoinTable(name = "contest_jury_link",
            joinColumns = @JoinColumn(name = "contest_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "jury_id", referencedColumnName = "id"))
    @ManyToMany
    private List<Jury> jury;

    @Column(name = "is_active", nullable = false)
    private Boolean is_active = false;

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public List<Jury> getJury() {
        return jury;
    }

    public void setJury(List<Jury> jury) {
        this.jury = jury;
    }

    public List<Partner> getPartners() {
        return partners;
    }

    public void setPartners(List<Partner> partners) {
        this.partners = partners;
    }

    public void setResults(LocalDate results) {
        this.results = results;
    }

    public LocalDate getResults() {
        return results;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public LocalDate getContest_end() {
        return contest_end;
    }

    public void setContest_end(LocalDate contest_end) {
        this.contest_end = contest_end;
    }

    public LocalDate getContest_start() {
        return contest_start;
    }

    public void setContest_start(LocalDate contest_start) {
        this.contest_start = contest_start;
    }

    public FileRef getImage() {
        return image;
    }

    public void setImage(FileRef image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}