package kz.beeline.beeplay.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;

@JmixEntity
@Table(name = "STREAM", indexes = {
        @Index(name = "IDX_STREAM_CATEGORY_ID", columnList = "category")
})
@Entity
public class Stream {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private Long id;

    @InstanceName
    @Column(name = "stream_id", nullable = false)
    private String stream_id;

    @JoinColumn(name = "category", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private StreamCategory category;

    public StreamCategory getCategory() {
        return category;
    }

    public void setCategory(StreamCategory category) {
        this.category = category;
    }

    public String getStream_id() {
        return stream_id;
    }

    public void setStream_id(String stream_id) {
        this.stream_id = stream_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}