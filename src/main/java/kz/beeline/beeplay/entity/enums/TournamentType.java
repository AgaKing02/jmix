
package kz.beeline.beeplay.entity.enums;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

public enum TournamentType implements EnumClass<String> {
    SOLO("SOLO"),
    DUO("DUO"),
    SQUAD("SQUAD"),
    ;
    private String id;

    TournamentType(String solo) {
        this.id=solo;
    }

    public static TournamentType fromId(String id) {
        for (TournamentType at : TournamentType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }

    @Override
    public String getId() {
        return this.id;
    }
}
