package kz.beeline.beeplay.service.impl;

import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import kz.beeline.beeplay.entity.Event;
import kz.beeline.beeplay.entity.Stream;
import kz.beeline.beeplay.repository.StreamRepository;
import kz.beeline.beeplay.entity.dto.StreamDTO;
import kz.beeline.beeplay.entity.dto.mapper.StreamMapper;
import kz.beeline.beeplay.service.interfaces.StreamService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class StreamServiceImpl implements StreamService {
    private final StreamRepository streamRepository;
    private final StreamMapper streamMapper;
    private final DataManager dataManager;

    public StreamServiceImpl(StreamRepository streamRepository, StreamMapper streamMapper, DataManager dataManager) {
        this.streamRepository = streamRepository;
        this.streamMapper = streamMapper;
        this.dataManager = dataManager;
    }

    public StreamDTO getStream(Long id){
        return streamMapper.toDto(streamRepository.findById(id).get());
    }


    @Override
    public Page<StreamDTO> getAll(Integer page, Integer size) {
        return streamRepository.findAll(PageRequest.of(page,size)).map(streamMapper::toDto);
    }

    @Override
    public StreamDTO getBySlug(String slug) throws IllegalStateException{
        return streamMapper.toDto(dataManager
                .load(Stream.class)
                .condition(PropertyCondition.equal("category.slug",slug)).one());
    }
}
