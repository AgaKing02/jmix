package kz.beeline.beeplay.service.impl;

import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import kz.beeline.beeplay.entity.Contest;
import kz.beeline.beeplay.entity.Event;
import kz.beeline.beeplay.entity.dto.ContestDTO;
import kz.beeline.beeplay.entity.dto.mapper.ContestMapper;
import kz.beeline.beeplay.repository.ContestRepository;
import kz.beeline.beeplay.service.interfaces.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ContestServiceImpl implements ContestService {
    private final ContestRepository contestRepository;

    private final ContestMapper contestMapper;

    private final DataManager dataManager;

    public ContestServiceImpl(ContestRepository contestRepository, ContestMapper contestMapper, DataManager dataManager) {
        this.contestRepository = contestRepository;
        this.contestMapper = contestMapper;
        this.dataManager = dataManager;
    }

    @Override
    public Page<ContestDTO> getContestPage(Pageable pageable) {
        return contestRepository.findAll(pageable).map(contestMapper::toDto);
    }

    @Override
    public ContestDTO getContestBySlug(String slug) throws IllegalStateException{
        return contestMapper.toDto(dataManager
                .load(Contest.class)
                .condition(PropertyCondition.equal("slug",slug)).one());
    }

    @Override
    public Page<ContestDTO> getContestPage(Integer page, Integer size) {
        return contestRepository.findAll(PageRequest.of(page != null ? page : 0, size != null ? size : 10)).map(contestMapper::toDto);
    }
}
