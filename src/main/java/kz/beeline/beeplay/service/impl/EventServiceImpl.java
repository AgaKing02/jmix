package kz.beeline.beeplay.service.impl;

import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import kz.beeline.beeplay.entity.Event;
import kz.beeline.beeplay.entity.dto.EventDTO;
import kz.beeline.beeplay.entity.dto.mapper.EventMapper;
import kz.beeline.beeplay.repository.EventRepository;
import kz.beeline.beeplay.service.interfaces.EventService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    private final EventMapper eventMapper;
    private final DataManager dataManager;

    public EventServiceImpl(EventRepository eventRepository, EventMapper eventMapper, DataManager dataManager) {
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
        this.dataManager = dataManager;
    }

    @Override
    public Page<EventDTO> getEvents(Integer page, Integer size) {
        return eventRepository.findAll(PageRequest.of(page != null ? page : 0, size != null ? size : 10)).map(eventMapper::toDto);
    }

    @Override
    public EventDTO getEventBySlug(String slug) throws IllegalStateException{

        return eventMapper.toDto(dataManager
                .load(Event.class)
                .condition(PropertyCondition.equal("slug",slug)).one());
    }
}
