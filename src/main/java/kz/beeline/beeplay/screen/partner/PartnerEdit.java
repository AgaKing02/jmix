package kz.beeline.beeplay.screen.partner;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Partner;

@UiController("Partner.edit")
@UiDescriptor("partner-edit.xml")
@EditedEntityContainer("partnerDc")
public class PartnerEdit extends StandardEditor<Partner> {
}