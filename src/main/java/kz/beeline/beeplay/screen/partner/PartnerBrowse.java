package kz.beeline.beeplay.screen.partner;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Partner;

@UiController("Partner.browse")
@UiDescriptor("partner-browse.xml")
@LookupComponent("partnersTable")
public class PartnerBrowse extends StandardLookup<Partner> {
}