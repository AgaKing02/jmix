package kz.beeline.beeplay.screen.sliderslide;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.SliderSlide;

@UiController("SliderSlide.edit")
@UiDescriptor("slider-slide-edit.xml")
@EditedEntityContainer("sliderSlideDc")
public class SliderSlideEdit extends StandardEditor<SliderSlide> {
}