package kz.beeline.beeplay.screen.sliderslide;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.SliderSlide;

@UiController("SliderSlide.browse")
@UiDescriptor("slider-slide-browse.xml")
@LookupComponent("sliderSlidesTable")
public class SliderSlideBrowse extends StandardLookup<SliderSlide> {
}