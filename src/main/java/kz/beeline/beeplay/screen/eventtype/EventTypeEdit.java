package kz.beeline.beeplay.screen.eventtype;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.EventType;

@UiController("EventType.edit")
@UiDescriptor("event-type-edit.xml")
@EditedEntityContainer("eventTypeDc")
public class EventTypeEdit extends StandardEditor<EventType> {
}