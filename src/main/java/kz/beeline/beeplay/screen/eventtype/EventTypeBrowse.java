package kz.beeline.beeplay.screen.eventtype;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.EventType;

@UiController("EventType.browse")
@UiDescriptor("event-type-browse.xml")
@LookupComponent("eventTypesTable")
public class EventTypeBrowse extends StandardLookup<EventType> {
}