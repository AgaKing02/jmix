package kz.beeline.beeplay.screen.link;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Link;

@UiController("Link.edit")
@UiDescriptor("link-edit.xml")
@EditedEntityContainer("linkDc")
public class LinkEdit extends StandardEditor<Link> {
}