package kz.beeline.beeplay.screen.link;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Link;

@UiController("Link.browse")
@UiDescriptor("link-browse.xml")
@LookupComponent("linksTable")
public class LinkBrowse extends StandardLookup<Link> {
}