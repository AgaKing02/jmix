package kz.beeline.beeplay.screen.contest;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Contest;

@UiController("Contest.browse")
@UiDescriptor("contest-browse.xml")
@LookupComponent("contestsTable")
public class ContestBrowse extends StandardLookup<Contest> {
}