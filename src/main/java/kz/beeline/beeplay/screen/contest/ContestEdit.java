package kz.beeline.beeplay.screen.contest;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Contest;

@UiController("Contest.edit")
@UiDescriptor("contest-edit.xml")
@EditedEntityContainer("contestDc")
public class ContestEdit extends StandardEditor<Contest> {
}