package kz.beeline.beeplay.screen.event;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Event;

@UiController("Event.edit")
@UiDescriptor("event-edit.xml")
@EditedEntityContainer("eventDc")
public class EventEdit extends StandardEditor<Event> {
}