package kz.beeline.beeplay.screen.event;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Event;

@UiController("Event.browse")
@UiDescriptor("event-browse.xml")
@LookupComponent("eventsTable")
public class EventBrowse extends StandardLookup<Event> {
}