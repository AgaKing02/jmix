package kz.beeline.beeplay.screen.jury;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Jury;

@UiController("Jury.edit")
@UiDescriptor("jury-edit.xml")
@EditedEntityContainer("juryDc")
public class JuryEdit extends StandardEditor<Jury> {
}