package kz.beeline.beeplay.screen.jury;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Jury;

@UiController("Jury.browse")
@UiDescriptor("jury-browse.xml")
@LookupComponent("juriesTable")
public class JuryBrowse extends StandardLookup<Jury> {
}