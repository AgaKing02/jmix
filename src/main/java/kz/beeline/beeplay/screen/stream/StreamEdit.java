package kz.beeline.beeplay.screen.stream;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Stream;

@UiController("Stream.edit")
@UiDescriptor("stream-edit.xml")
@EditedEntityContainer("streamDc")
public class StreamEdit extends StandardEditor<Stream> {
}