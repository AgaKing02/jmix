package kz.beeline.beeplay.screen.stream;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Stream;

@UiController("Stream.browse")
@UiDescriptor("stream-browse.xml")
@LookupComponent("streamsTable")
public class StreamBrowse extends StandardLookup<Stream> {
}