package kz.beeline.beeplay.screen.social;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Social;

@UiController("Social.browse")
@UiDescriptor("social-browse.xml")
@LookupComponent("socialsTable")
public class SocialBrowse extends StandardLookup<Social> {
}