package kz.beeline.beeplay.screen.social;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Social;

@UiController("Social.edit")
@UiDescriptor("social-edit.xml")
@EditedEntityContainer("socialDc")
public class SocialEdit extends StandardEditor<Social> {
}