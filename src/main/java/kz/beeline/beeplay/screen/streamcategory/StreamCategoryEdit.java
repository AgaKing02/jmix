package kz.beeline.beeplay.screen.streamcategory;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.StreamCategory;

@UiController("StreamCategory.edit")
@UiDescriptor("stream-category-edit.xml")
@EditedEntityContainer("streamCategoryDc")
public class StreamCategoryEdit extends StandardEditor<StreamCategory> {
}