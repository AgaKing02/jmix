package kz.beeline.beeplay.screen.streamcategory;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.StreamCategory;

@UiController("StreamCategory.browse")
@UiDescriptor("stream-category-browse.xml")
@LookupComponent("streamCategoriesTable")
public class StreamCategoryBrowse extends StandardLookup<StreamCategory> {
}