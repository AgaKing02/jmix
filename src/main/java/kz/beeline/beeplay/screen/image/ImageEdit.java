package kz.beeline.beeplay.screen.image;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Image;

@UiController("Image.edit")
@UiDescriptor("image-edit.xml")
@EditedEntityContainer("imageDc")
public class ImageEdit extends StandardEditor<Image> {
}