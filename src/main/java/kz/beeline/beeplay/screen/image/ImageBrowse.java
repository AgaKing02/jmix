package kz.beeline.beeplay.screen.image;

import io.jmix.ui.screen.*;
import kz.beeline.beeplay.entity.Image;

@UiController("Image.browse")
@UiDescriptor("image-browse.xml")
@LookupComponent("imagesTable")
public class ImageBrowse extends StandardLookup<Image> {
}