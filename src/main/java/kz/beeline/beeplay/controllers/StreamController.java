package kz.beeline.beeplay.controllers;

import kz.beeline.beeplay.entity.Stream;
import kz.beeline.beeplay.entity.dto.EventDTO;
import kz.beeline.beeplay.entity.dto.StreamDTO;
import kz.beeline.beeplay.repository.StreamRepository;
import kz.beeline.beeplay.service.impl.StreamServiceImpl;
import kz.beeline.beeplay.service.interfaces.StreamService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class StreamController {

    private final StreamService service;

    public StreamController(StreamService service) {
        this.service = service;
    }

    @GetMapping("/streams")
    public ResponseEntity<List<StreamDTO>> getStreams(@RequestParam(name = "page_size", required = false,defaultValue = "10") Integer size,
                                                      @RequestParam(name = "page", required = false,defaultValue = "0") Integer page) {

        return ResponseEntity.ok(service.getAll(page,size).getContent());

    }

    @GetMapping("/streams/{slug}")
    public ResponseEntity<?> getStream(@PathVariable("slug")String slug ){
        StreamDTO streamDTO;
        try {
            streamDTO=service.getBySlug(slug);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Stream doesn't exist");
        }
        return ResponseEntity.ok(streamDTO);
    }

}
