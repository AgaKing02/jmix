package kz.beeline.beeplay.controllers;

import kz.beeline.beeplay.entity.dto.EventDTO;
import kz.beeline.beeplay.service.interfaces.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class EventController {
    @Autowired
    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/events")
    public ResponseEntity<List<EventDTO>> getAllContests(
            @RequestParam(name = "page_size", required = false, defaultValue = "10") Integer size,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
        return ResponseEntity.ok(eventService.getEvents(page, size).getContent());
    }

    @GetMapping("/events/{slug}")
    public ResponseEntity<?> getContestBySlug(@PathVariable("slug") String slug) {
        EventDTO eventDTO;
        try {
            eventDTO = eventService.getEventBySlug(slug);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Event doesn't exist");
        }
        return ResponseEntity.ok(eventDTO);
    }
}
