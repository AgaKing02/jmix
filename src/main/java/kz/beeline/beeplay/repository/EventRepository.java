package kz.beeline.beeplay.repository;

import io.jmix.core.repository.JmixDataRepository;
import kz.beeline.beeplay.entity.Event;
import kz.beeline.beeplay.entity.dto.EventDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JmixDataRepository<Event, Long> {
    EventDTO findBySlug(String slug);
}
