package kz.beeline.beeplay.repository;

import io.jmix.core.repository.JmixDataRepository;
import kz.beeline.beeplay.entity.Contest;
import org.springframework.stereotype.Repository;

@Repository
public interface ContestRepository extends JmixDataRepository<Contest, Long> {
    Contest findContestBySlug(String slug);
}
