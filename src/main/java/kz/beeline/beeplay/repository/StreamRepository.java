package kz.beeline.beeplay.repository;

import io.jmix.core.repository.JmixDataRepository;
import kz.beeline.beeplay.entity.Stream;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StreamRepository extends JmixDataRepository<Stream, Long> {
    Stream getStreamByCategorySlug(String category_slug);
}
